# Unity SRT Subtitles Manager #

A simple SRT subtitles manager that allows you to easily import subtitles to your cut-scenes.

### How to use it? ###

* Initialize *SubtitleManager* class
* Use *processLines* method to read your SRT text. Note that *subsLines* parameter is an array of strings so you can either read lines from file or just copy/paste it if you wish.
* On starting your cut-scene, run *displayLines* as a coroutine. Note that the *textMesh* parameter is *TextMeshPro* type, so the subs are working correctly with this type only, but this is easily changable.
* Enjoy subtitles in your cut-scene!

### The format it supports ###

Just to be clear, the subtitles supported by this manager should look like below:

    
    1 
    00:00:03,420 --> 00:00:05,380 
    SOME TEXT
     
    2 
    00:00:06,423 --> 00:00:12,679 
    Some other text
    This time with another line

...and so on!

### Feel free to use it! ###
