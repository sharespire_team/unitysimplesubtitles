﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Subtitle
{
    private int id;
    private float timeStart;
    private float timeStop;
    private string text;

    public Subtitle(int id, float timeStart, float timeStop, string text)
    {
        this.id = id;
        this.TimeStart = timeStart;
        this.TimeStop = timeStop;
        this.Text = text;
    }

    public float TimeStart { get => timeStart; set => timeStart = value; }
    public float TimeStop { get => timeStop; set => timeStop = value; }
    public string Text { get => text; set => text = value; }
}


public class SubtitleManager
{
    public List<Subtitle> subtitles;
    private List<string> lines = new List<string>();


    public SubtitleManager()
    {
        subtitles = new List<Subtitle>();
    }

    public void processLines(string[] subsLines)
    {
        //int currentLineId = 0;
        Subtitle sub;
        string text;

        foreach (string line in subsLines)
        {
            if (line != "")
            {
                lines.Add(line);
            }
            else
            {
                if (lines.Count > 2)
                {
                    string[] separators = { " --> " };
                    string[] times = lines[1].Split(separators, System.StringSplitOptions.RemoveEmptyEntries);

                    sub = new Subtitle(int.Parse(lines[0]),
                        Convert.ToSingle(TimeSpan.Parse(times[0]).TotalSeconds),
                        Convert.ToSingle(TimeSpan.Parse(times[1]).TotalSeconds),
                        string.Join(Environment.NewLine, new List<string>(lines.GetRange(2, lines.Count - 2))));

                    subtitles.Add(sub);
                    lines.Clear();
                }

            }

        }
    }

    public IEnumerator displayLines(TextMeshPro textMesh)
    {
        float previousSeconds = 0;
        textMesh.text = "";
        foreach (Subtitle s in subtitles)
        {
            yield return new WaitForSeconds(s.TimeStart - previousSeconds);
            textMesh.text = s.Text;
            yield return new WaitForSeconds(s.TimeStop - s.TimeStart);
            textMesh.text = "";
            previousSeconds = s.TimeStop;
        }
    }
}
